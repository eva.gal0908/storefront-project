import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ItemDataService} from "../Services/item-data.service";

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.scss']
})
export class ItemDetailComponent implements OnInit {

  public currentId: number = 0;
  public item: any;
  public singleDetail: boolean = true;

  constructor(
    public route: ActivatedRoute,
    public itemData: ItemDataService,
  ) { }

  ngOnInit(): void {
    this.getItem();
  }

  getItem(): void {
    this.currentId = Number(this.route.snapshot.paramMap.get('id'));
    this.itemData.items.forEach( data => {
      if(data.id == this.currentId) {
        this.item = data;
      }
    })
  }

}
