import {Component, DoCheck, Input, OnChanges, OnInit} from '@angular/core';
import {ItemDataService} from "../Services/item-data.service";


@Component({
  selector: 'app-item-data',
  templateUrl: './item-data.component.html',
  styleUrls: ['./item-data.component.scss']
})
export class ItemDataComponent implements OnInit {

  @Input() item: any;
  @Input() singleDetail: boolean = false;

  constructor(public itemData: ItemDataService) {
  }

  ngOnInit(): void {

  }


}
