import {Component, DoCheck, Input, OnInit} from '@angular/core';
import {ItemDataService} from "../Services/item-data.service";

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit, DoCheck {

  public itemList: any;

  constructor(public itemData: ItemDataService) {
  }

  ngOnInit(): void {
  }

  ngDoCheck() {
    this.itemList = this.itemData.searchItemsSet;
  }

}
