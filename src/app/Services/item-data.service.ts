import {Injectable} from '@angular/core';
import items from '../../assets/Json/itemsData.json';
import filters from '../../assets/Json/filters.json'


@Injectable({
  providedIn: 'root'
})
export class ItemDataService {

  public items = items;
  public filters = filters;
  public selectedItem: Array<any> = [];
  public selectedFilter: Array<any> = [];
  public searchItemsSet: Set<any> = new Set<any>();
  public searchItems: Array<any> = [];
  public filterSet: Set<any> = new Set<any>();
  public applyFilterSet: Set<any> = new Set<any>()
  public display: boolean = false;

  constructor() {
    this.items.forEach(item => this.searchItemsSet.add(item));
  }

  displayFilters() {
    this.display = !this.display;
  };

  addFilter(filter: any, index: any) {
    this.applyFilterSet.add(filter);
    this.selectedFilter.push(index);
  }

  applyFilter() {
    if (this.applyFilterSet.size) {
      this.searchItemsSet.clear();
    }
    this.applyFilterSet.forEach(item => {
      this.items.forEach(data => {
        if (data.filter.includes(item.name)) {
          this.searchItemsSet.add(data);
        }
      });
    });
    this.clearFilter();
  }

  clearFilter() {
    this.applyFilterSet.clear();
    this.selectedFilter = [];
  }

  removeFilter() {
    this.selectedFilter = [];
    this.searchItemsSet.clear();
    this.items.forEach(item => this.searchItemsSet.add(item))
  }

  addFilterItem(id: any, data: any, index: any): void {
    this.selectedItem.push(index);
    this.items.forEach(item => {
      if (item.id == id) {
        item.filter.forEach(filterIndex => {
          this.filterSet.add(filterIndex);
        })
        this.filterSet.add(data);
      }
    })
  }

  confirmFilterItem(id: any): void {
    this.items.forEach(item => {
      if (item.id == id && this.filterSet.size) {
        item["filter"] = [];
        this.filterSet.forEach(data => {
          item["filter"].push(data);
        })
      }
    })
    this.selectedItem = [];
  }

  removeFiltersItem() {
    this.selectedItem = [];
    this.filterSet.clear();
  }
}
